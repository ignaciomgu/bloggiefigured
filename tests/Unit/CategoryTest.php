<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryTest extends TestCase
{
    /**
     * Creating category without credentials
     *
     */
    public function testCreateCategoryWithMiddleware()
    {
        $data = [
                   'name' => "categoryTest"
                ];

        $response = $this->json('POST', '/admin/categories',$data);
        $response->assertStatus(401);

    }

    /**
     * Create new category authenticated with simple user
     */
    public function testCreateCategoryWithoutAdmin()
    {
            \App\User::where('email', 'a@a.com')->delete();
            $user = \App\User::create([
                'name' => "a",
                'email' => "a@a.com",
                'password' => "secret",
                'is_admin' => "0",
            ]);
            $data = [
                'name' => "category1"
            ];
            $response = $this->actingAs($user, 'web')->json('POST', '/admin/categories',$data);
            $response->assertStatus(401);
            \App\User::where('email', 'a@a.com')->delete();
      }

    /**
     * Create new category authenticated with admin
     */
    public function testCreateCategory()
    {
            \App\User::where('email', 'a@a.com')->delete();
            $user = \App\User::create([
                'name' => "a",
                'email' => "a@a.com",
                'password' => "secret",
                'is_admin' => "1",
            ]);
            $data = [
                'name' => "category1"
            ];
            $response = $this->actingAs($user, 'web')->json('POST', '/admin/categories',$data);
            $response->assertStatus(302);
            \App\User::where('email', 'a@a.com')->delete();
            \App\Models\Category::where('name', 'category1')->delete();
      }

     /**
     * Destroy created category authenticated with admin
     */
    public function testDeleteCategory()
    {
            \App\User::where('email', 'a@a.com')->delete();
            $user = \App\User::create([
                'name' => "a",
                'email' => "a@a.com",
                'password' => "secret",
                'is_admin' => "1",
            ]);
            \App\Models\Category::where('name', 'category1')->delete();
            $category = \App\Models\Category::create([
                'name' => "category1"
            ]);
            $data = [
                '_method' => "DELETE"
            ];
            $response = $this->actingAs($user, 'web')->json('POST', '/admin/categories/'.$category->id,$data);
            $response->assertStatus(302);
            \App\User::where('email', 'a@a.com')->delete();
      }  
       
    /**
     * Destroy created category authenticated with simple user
     */
    public function testDeleteCategoryWhitoutAdmin()
    {
            \App\User::where('email', 'a@a.com')->delete();
            $user = \App\User::create([
                'name' => "a",
                'email' => "a@a.com",
                'password' => "secret",
                'is_admin' => "0",
            ]);
            \App\Models\Category::where('name', 'category1')->delete();
            $category = \App\Models\Category::create([
                'name' => "category1"
            ]);
            $data = [
                '_method' => "DELETE"
            ];
            $response = $this->actingAs($user, 'web')->json('POST', '/admin/categories/'.$category->id,$data);
            $response->assertStatus(401);
            \App\User::where('email', 'a@a.com')->delete();
            \App\Models\Category::where('id', $category->id)->delete();
      }

     /**
     * Edit created category authenticated with admin
     */
    public function testEditCategory()
    {
            \App\User::where('email', 'a@a.com')->delete();
            $user = \App\User::create([
                'name' => "a",
                'email' => "a@a.com",
                'password' => "secret",
                'is_admin' => "1",
            ]);
            \App\Models\Category::where('name', 'category1')->delete();
            $category = \App\Models\Category::create([
                'name' => "category1"
            ]);
            $data = [
                'name' => "category2",
                '_method' => "PUT"
            ];
            $response = $this->actingAs($user, 'web')->json('POST', '/admin/categories/'.$category->id,$data);
            $response->assertStatus(302);
            \App\User::where('email', 'a@a.com')->delete();
            \App\Models\Category::where('id', $category->id)->delete();
      }

    /**
     * Edit created category authenticated with simple user
     */
    public function testEditCategoryWhitoutAdmin()
    {
        \App\User::where('email', 'a@a.com')->delete();
        $user = \App\User::create([
            'name' => "a",
            'email' => "a@a.com",
            'password' => "secret",
            'is_admin' => "0",
        ]);
        \App\Models\Category::where('name', 'category1')->delete();
        $category = \App\Models\Category::create([
            'name' => "category1"
        ]);
        $data = [
            'name' => "category2",
            '_method' => "PUT"
        ];
        $response = $this->actingAs($user, 'web')->json('POST', '/admin/categories/'.$category->id,$data);
        $response->assertStatus(401);
        \App\User::where('email', 'a@a.com')->delete();
        \App\Models\Category::where('id', $category->id)->delete();

      }
}
