<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PostsTest extends TestCase
{
    /**
     * Creating post without credentials
     *
     */
    public function testCreatePostWithMiddleware()
    {
        $data = [
                   'title' => "Test",
                   'body' => "Test",
                   'category_id' => "20",
                   'tags' => ["tag1"],
                ];

        $response = $this->json('POST', '/admin/posts',$data);
        $response->assertStatus(401);

    }

    /**
     * Create new post authenticated with simple user
     */
    public function testCreatePostWhithoutAdmin()
    {
            \App\User::where('email', 'a@a.com')->delete();
            $user = \App\User::create([
                'name' => "a",
                'email' => "a@a.com",
                'password' => "secret",
                'is_admin' => "0",
            ]);
            \App\Models\Category::where('name', 'category1')->delete();
            $category = \App\Models\Category::create([
                'name' => "category1"
            ]);
            $data = [
                'title' => "TestTest",
                'body' => "TestTest",
                'category_id' => $category->id,
                'tags' => ["tag1"],
            ];
            $response = $this->actingAs($user, 'web')->json('POST', '/admin/posts',$data);
            $response->assertStatus(401);
            \App\Models\Category::where('name', 'category1')->delete();
            \App\Models\Tag::where('name', 'tag1')->delete();
            \App\User::where('email', 'a@a.com')->delete();
            
      }

    /**
     * Create new post authenticated with admin
     */
    public function testCreatePost()
    {
            \App\User::where('email', 'a@a.com')->delete();
            $user = \App\User::create([
                'name' => "a",
                'email' => "a@a.com",
                'password' => "secret",
                'is_admin' => "1",
            ]);
            \App\Models\Category::where('name', 'category1')->delete();
            $category = \App\Models\Category::create([
                'name' => "category1"
            ]);
            $data = [
                'title' => "TestTest",
                'body' => "TestTest",
                'category_id' => $category->id,
                'tags' => ["tag1"],
            ];
            $response = $this->actingAs($user, 'web')->json('POST', '/admin/posts',$data);
            $response->assertStatus(302);
            \App\Models\Post::where('title', 'Test')->delete();
            \App\Models\Category::where('name', 'category1')->delete();
            \App\Models\Tag::where('name', 'tag1')->delete();
            \App\User::where('email', 'a@a.com')->delete();
            
      }
       
}
