<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Services\PostService;
use App\Models\Comment;
use App\Models\Post;

/**
 * This controller manage the post views and full text searching for unkwown users and auth users 
 * and the comments in a posts by the last one
 */
class BlogController extends Controller
{
    /**
     * Full texts seraching in all posts
     * 
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //Here must be necesary limit the length of the text search.
        $postService = new PostService();
        $posts = $postService->search($request);          
        error_log($posts);    
        return view('frontend.index', compact('posts'));
    }

    /**
     * Show a Post
     * 
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function post(Post $post)
    {
        try {
                $post = $post->load(['comments.user', 'tags', 'user', 'category']);
        
            } catch (\Exception $e) {
                abort(500);
            }      
        return view('frontend.post', compact('post'));
    }

     /**
     * A auth user make a comment into a Post
     * 
     * @param  Request $request
     * @param  Post $post
     * @return \Illuminate\Http\Response
     */
    public function comment(Request $request, Post $post)
    {
        $this->validate($request, ['body' => 'required']);
        $postService = new PostService();
        $postService->comment($request, $post);      
        
        flash()->overlay('Comment successfully created');
        return redirect("/posts/{$post->id}");
    }
}
