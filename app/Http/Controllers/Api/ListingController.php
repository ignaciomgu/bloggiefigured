<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Models\Tag;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ListingController extends Controller
{
    /**
     * Get all tags (paginate 10)
     */
    public function tags()
    {
        $tags = Tag::paginate(10);

        return $tags;
    }

    /**
     * Get all categories (paginate 10)
     */
    public function categories()
    {
        $categories = Category::paginate(10);

        return $categories;
    }

    /**
     * Get all users (paginate 10)
     */
    public function users()
    {
        $users = User::paginate(10);

        return $users;
    }
}
