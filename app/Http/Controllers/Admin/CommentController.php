<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\CommentService;
use App\Models\Comment;

/*
    This controller handles the view and delete for comments
*/
class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $commentService = new CommentService();
        $comments = $postService->getComments();

        return view('admin.comments.index', compact('comments'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        $commentService = new CommentService();
        $comments = $postService->destroy($comment);
        flash()->overlay('Comment deleted successfully.');

        return redirect('/admin/comments');
    }
}
