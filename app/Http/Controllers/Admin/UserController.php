<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Services\UserService;
use App\User;

/*
    This controller handles the view and delete for users
*/
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userService = new UserService();
        $users = $userService->getUsers();    
        return view('admin.users.index', compact('users'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $userService = new UserService();
        $userService->destroy($user);        

        flash()->overlay('User deleted successfully.');  
        return redirect('/admin/users');
    }
}
