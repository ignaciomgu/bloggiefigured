<?php

namespace App\Http\Controllers\Admin;
use App\Models\Tag;
use App\Models\Post;
use App\User;
use App\Models\PostMongo;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Services\PostService;

/*
    This controller handles the creation of new post as well as their
    other actions.
*/
class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $postService = new PostService();
        $posts = $postService->getPosts();
        return view('admin.posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $postService = new PostService();
        $categories = $postService->getCategories();
        $tags = $postService->getTags();
       
        return view('admin.posts.create', compact('categories', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {

        $postService = new PostService();
        $postService->store($request);
        flash()->overlay('Post created successfully.');
        return redirect('/admin/posts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        try {
             $post = $post->load(['user', 'category', 'tags', 'comments']);
            } catch (\Exception $e) {
                abort(500);
            }    
        return view('admin.posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $postService = new PostService();
        $categories = $postService->getCategories();
        $tags = $postService->getTags();

        return view('admin.posts.edit', compact('post', 'categories', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, Post $post)
    {
        $postService = new PostService();
        $postService->update($request,$post);
        flash()->overlay('Post updated successfully.');
        return redirect('/admin/posts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $postService = new PostService();
        $postService->destroy($post);
        flash()->overlay('Post deleted successfully.');
        return redirect('/admin/posts');
    }

    /**
     * Publish the specified resource from storage.
     * 
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function publish(Post $post) 
    {
        $postService = new PostService();
        $postService->publish($post);
        flash()->overlay('Post changed successfully.');
        return redirect('/admin/posts');
    }
}
