<?php

namespace App\Models;
use App\Models\TagMogo;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
/**
 * Desnormalized Post for storing in Mongo DB
 */
class PostMongo extends Eloquent
{

    protected $connection = 'mongodb';

    protected $collection = 'post_mongos';

    /**
     * title: post title
     * body: post body (description)
     * idR: Post ID in relational Database (Post reference)
     * category: pseudo object of Category
     * user: pseudo object of User
     * tags: list of string tags
     * comments: list of string comments
     * is_published: if it is public or not.
     */
    protected $fillable = [
        'title',
        'body',
        'idR',
        'category',
        'user',
        'tags',
        'comments',
        'is_published'
    ];


}
