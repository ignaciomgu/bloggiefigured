<?php

namespace App\Models;

use App\Models\Post;
use Illuminate\Database\Eloquent\Model;

/**
 * Entity that represent the object to store
 */
class Tag extends Model
{
    protected $fillable = ['name'];

    public function posts()
    {
        return $this->belongsToMany(Post::class)->withTimestamps();
    }
}
