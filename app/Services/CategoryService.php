<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\PostMongo;
use Illuminate\Support\Facades\DB;

/*
    This service interacts with the database in "category" actions
*/
class CategoryService
{
    /**
     * Get a listing of the categories; with size.
     *
     */
    public function getCategories()
    {  
        try {
               return Category::withCount('posts')->paginate(10);
            } catch (\Exception $e) {
                abort(500);
            }    
    }

    /**
     * Store a newly created category in storage.
     *
     */
    public function store(Request $request)
    {
        try {
                DB::beginTransaction();
                Category::create(['name' => $request->name]);
                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                abort(500);
            }    
    }

    /**
     * Update the specified category in storage.
     *
     */
    public function update(Request $request, Category $category)
    {
        
        try {
                DB::beginTransaction();
                $category->update($request->all());
                $data = ['category' =>  ['id' => $category->id, 'name' => $category->name]];
                PostMongo::where('category.id', $category->id)->update($data, array('upsert' => true));
                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                abort(500);
            }    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        try {
                DB::beginTransaction(); 
                PostMongo::where('category.id', $category->id)->delete();
                $category->delete();
                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                abort(500);
            }    
    }
}
