<?php

namespace App\Services;

use App\Models\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/*
    This controller handles the ABM for tags
*/
class TagService
{
    /**
     * Display a listing of the tags.
     *
     */
    public function getTags()
    {
        
        try {
            return Tag::paginate(10);
        } catch (\Exception $e) {
            abort(500);
        }    

    }

    /**
     * Store a newly created tag in storage.
     *
     */
    public function store(Request $request)
    {
        try {
            Tag::create(['name' => $request->name]);
        } catch (\Exception $e) {
            abort(500);
        }   
    }

    /**
     * Update the specified tag in storage.
     *
     */
    public function update(Request $request, Tag $tag)
    {
        try {
            $tag->update($request->all());
        } catch (\Exception $e) {
            abort(500);
        }  
    }

    /**
     * Remove the specified tag from storage.
     *
     */
    public function destroy(Tag $tag)
    {
        try {
            $tag->delete();
        } catch (\Exception $e) {
            abort(500);
        }  
    }
}
