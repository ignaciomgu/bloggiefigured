<?php

namespace App\Services;
use App\Models\Tag;
use App\Models\Post;
use App\User;
use App\Models\PostMongo;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

/*
    This service interacts with the database in "post" actions
*/
class PostService
{
    /**
     * Get all posts and their fileds; with pagination
     */
    public function getPosts()
    {  
        try {
            return Post::with(['user', 'category', 'tags', 'comments'])->paginate(10);
        } catch (\Exception $e) {
            abort(500);
        }
    }

    /**
     * Get all the categories
     */
    public function getCategories()
    {
        try {
            return Category::pluck('name', 'id')->all();
        } catch (\Exception $e) {
            abort(500);
        }    
    }

    /**
     * Get all the tags
     */
    public function getTags()
    {
        try {
            return Tag::pluck('name', 'name')->all();
        } catch (\Exception $e) {
            abort(500);
        }    
    }

    /**
     * Create a new Post
     */
    public function store(PostRequest $request)
    {
        try {
                DB::beginTransaction();
                $post = Post::create([
                    'title'       => $request->title,
                    'body'        => $request->body,
                    'category_id' => $request->category_id
                ]);

                $tagsId = collect($request->tags)->map(function($tag) {
                    return Tag::firstOrCreate(['name' => $tag])->id;
                });
                $post->tags()->attach($tagsId);
                $category = Category::where('id', $request->category_id)->first();
                $postMongo = PostMongo::create([
                    'title'       => $request->title,
                    'body'        => $request->body,
                    'category' =>  ['id' => $category->id, 'name' => $category->name],
                    'user' => ['id' => $post->user->id, 'name' => $post->user->name],
                    'tags' => $request->tags,
                    'comments' => array(),
                    'idR' => $post->id,
                    'is_published' => false
                ]);
                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                abort(500);
            }    
    }  

    /**
     * Modify a Post
     */
    public function update(PostRequest $request, Post $post)
    {
        try {
                DB::beginTransaction();    
                $post->update([
                    'title'       => $request->title,
                    'body'        => $request->body,
                    'category_id' => $request->category_id
                ]);

                $tagsId = collect($request->tags)->map(function($tag) {
                    return Tag::firstOrCreate(['name' => $tag])->id;
                });

                $post->tags()->sync($tagsId);

                $category = Category::where('id', $request->category_id)->first();
                $findPostMongo = PostMongo::where('idR', $post->id)->first();
                $findPostMongo->update([
                    'title'       => $request->title,
                    'body'        => $request->body,
                    'category' =>  ['id' => $category->id, 'name' => $category->name],
                    'user' =>  ['id' => $post->user->id, 'name' => $post->user->name],
                    'tags' => $request->tags,
                    'idR' => $post->id,
                    'is_published' => false
                ]);
                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                abort(500);
            }    
    }

    /**
     * Delete a Post
     */
    public function destroy(Post $post)
    {
        try {
                DB::beginTransaction();  
                $findPostMongo = PostMongo::where('idR', $post->id)->first();
                $findPostMongo->delete();

                $post->delete();
                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                abort(500);
            }    
    }

    /**
     * Make visible a Post
     */
    public function publish(Post $post) 
    {
        try {
                DB::beginTransaction();  
                $post->is_published = !$post->is_published;
                $post->save();
                $data = array ('is_published' => $post->is_published) ;
                PostMongo::where('idR', $post->id)->update($data, array('upsert' => true));
                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                abort(500);
            }    
    }

    /**
     * Looking for a posts contain the text searched in some of their fields
     */
    public function search(Request $request)
    {
        try {
                           $search = $request->search;
                           $posts = DB::connection('mongodb')->collection('post_mongos')
                                ->where('title', 'like', "%$search%")
                                ->orWhere('body', 'like', "%$search%")
                                ->orWhere('category.name', 'like', "%$search%")
                                ->orWhere('user.name', 'like', "%$search%")
                                ->orWhere('tags', 'all',  ["%$search%"])
                                ->orWhere('comments', 'all', ["%$search%"])
                                ->orWhere('is_published', true)->get();
                            error_log($search);
                            return $posts;                
            } catch (\Exception $e) {
                abort(500);
            }    
    }

    /**
     * Add new comment to a post
     */
    public function comment(Request $request, Post $post)
    {
        try {
                DB::beginTransaction();
                $post->comments()->create([
                    'body' => $request->body
                ]);

                PostMongo::where('idR', $post->id)->push('comments', $request->body, array('upsert' => true));
                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                abort(500);
            }    
    }
}
