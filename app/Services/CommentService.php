<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Comment;

/*
    This service interacts with the database in "Comment" actions
*/
class CommentService
{
    /**
     * Display a listing of the resource.
     *
     */
    public function getComments()
    { 
        try {
            return Comment::with('post')->paginate(10);
        } catch (\Exception $e) {
            abort(500);
        }    
    }

    /**
     * Remove the specified resource from storage.
     *
     */
    public function destroy(Comment $comment)
    {
        try {
            $comment->delete();
        } catch (\Exception $e) {
            abort(500);
        }
    }
}
