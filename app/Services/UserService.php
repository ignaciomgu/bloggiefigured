<?php

namespace App\Services;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class UserService
{
    /**
     * Display a listing of the users.
     *
     */
    public function getUsers()
    {
        try {
                return User::withCount('posts')->paginate(10);
            } catch (\Exception $e) {
                abort(500);
            }    
    }

    /**
     * Remove the specified user from storage.
     *
     */
    public function destroy(User $user)
    {
        try {
                DB::beginTransaction();
                $user->delete();
                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                abort(500);
            }    
    }
}
