<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        User::create([
            'name'     => 'Ignacio Gallardo',
            'email'    => 'admin@admin.com',
            'password' => bcrypt('admin'),
            'is_admin' => true
        ]);
    }
}
