@extends('layouts.app')

@section('content')
    <div class="container">

        @include('frontend._search')

        <div class="row">

            <div class="col-md-12">
                @forelse ($posts as $post)
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            {{ $post['title'] }} - <small>by {{ $post['user']['name'] }}</small>

                            <span class="pull-right">
                                {{ $post['created_at'] }}
                            </span>
                        </div>
                        <div class="panel-body">
                            <p>{{ str_limit($post['body'], 200) }}</p>
                            <p>
                                Tags:
                                @forelse ($post['tags'] as $tag)
                                    <span class="label label-default">{{ $tag }}</span>
                                @empty
                                    <span class="label label-danger">No tag found.</span>
                                @endforelse
                            </p>
                            <p>
                                <span class="btn btn-sm btn-success">{{ $post['category']['name'] }}</span>
                                <span class="btn btn-sm btn-info">Comments <span class="badge">{{ sizeof($post['comments']) }}</span></span>

                                <a href="{{ url("/posts/{$post['idR']}") }}" class="btn btn-sm btn-primary">See more</a>
                            </p>
                        </div>
                    </div>
                @empty
                    <div class="panel panel-default">
                        <div class="panel-heading">The list is empty</div>

                        <div class="panel-body">
                            <p>There are not posts</p>
                        </div>
                    </div>
                @endforelse

                <div align="center">
               
                </div>

            </div>

        </dev>
    </dev>
@endsection
