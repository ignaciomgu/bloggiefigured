# Bloggie

A simple blog app to demonstrate a proof of concept with: 
<p align="center"> <img width="200px" src="https://laravel.com/assets/img/components/logo-laravel.svg"> </p>
<p align="center"> <img width="200px" src="https://zdnet3.cbsistatic.com/hub/i/r/2018/02/16/8abdb3e1-47bc-446e-9871-c4e11a46f680/resize/370xauto/8a68280fd20eebfa7789cdaa6fb5eff1/mongo-db-logo.png"></p> 
<p align="center"> <img width="200px" src="https://www.redeszone.net/app/uploads/2014/10/mysql_logo.jpg"></p> 
<p align="center"> <img width="200px" src="https://damiandeluca.com.ar/wp-content/uploads/2018/03/vuejs.jpg"></p> 

## What can I do with this app?

Proof of concepts: Simple users can read/comment all posts, and the Admin users create/delete/update posts.

Simple Users can: 

- View all public blog posts.
- Register/Login/logout
- Make comments in all public blog posts.
- Full text search by: comment contents, creator, category names, tag names, post body contents and post title contents. 

Admin Users can:

- Login/logout.
- View number of all Posts, Categories and Users.
- Make CRUD operations in all Posts, Categories and Users.
- Make comments in all blog posts.
- Full text search by: comment contents, creator, category names, tag names, post body contents and post title contents. 

## Demo (with a gif)

* [Go to the Gif](https://gifyu.com/image/v2rK) - I hosted it in  [Gifyu](https://gifyu.com) :cloud: because gif file is hard. (Or you can find gif file named ``` gifDemo.gif ``` into the bloggiefigured directory)

## Environment Installation

- I recommend using the [XAMPP](https://www.apachefriends.org/es/index.html) program that contains all the settings we require: Apache + MariaDB + PHP + Perl.
- Install [Composer](https://getcomposer.org/) on your computer. Download the .exe file and next ... next ... next ... (if you have linux or mac you can find the steps into url)
- Execute this command in some console: ``` composer global require "laravel/installer" ```, it will install Laravel globally on your computer.
- Execute XAMPP MySql server and create new schema (with schema name = bloggie if you want to reuse the project default configuration). You can manage the MySql Database with [Workbench IDE](https://dev.mysql.com/downloads/workbench/).
- Make sure you have the MongoDB PHP driver installed. You can find installation instructions at http://php.net/manual/en/mongodb.installation.php OR AT https://www.youtube.com/watch?v=9gEPiIoAHo8 (THE BEST EXPLANATION) .
- Download [MongoDB database](https://www.mongodb.com/download-center/community). You can manage the MongoDB Database with [MongoDB Compass IDE](https://www.mongodb.com/download-center/compass). Before execute MongoDB compass, you have to init mongodb server. 
- Execute this command in some console: ``` composer global require jenssegers/mongodb ```


## Project Installation

```
git clone bloggiefigured
cd bloggiefigured
composer install
```

Edit ```.env``` file with the required data:

```
DB_CONNECTION=mysql
DB_HOST= 127.0.0.1 or where it is running
DB_PORT= 3306 or where it is listening
DB_DATABASE= schema name
DB_USERNAME= username if it has
DB_PASSWORD= password if it has
```

```
DB_MONGO_CONNECTION=mongodb
DB_MONGO_HOST= 127.0.0.1 or where it is running
DB_MONGO_PORT= 27017 or where it is listening
DB_MONGO_DATABASE=bloggie
DB_MONGO_USERNAME= username if it has
DB_MONGO_PASSWORD= password if it has
```

```
php artisan key:generate
php artisan migrate
php artisan db:seed
php artisan serve
```

```
And the web app will run in -> http://localhost:8000
```

## Login info

User: admin@admin.com 
Password: admin

## Architecture

<img src="https://gitlab.com/ignaciomgu/bloggiefigured/raw/master/architecture.png">

The web app stores all logic model in MySql Database, and in MongoDB stores a desnormalized Post Table version with the aim of using for full text serching.

## Classes Diagram (resumed)

<img src="https://gitlab.com/ignaciomgu/bloggiefigured/raw/master/diagram.png">

- Post: It represents the blog posts.
- Category: We have Post Categories (One category to many posts).
- Comment: Post comments (Many comments to one Post).
- Tag: Using tu hashtagging the posts (Many to many).
- User: It represents the actor.
- PostMongo: Desnormalized Post version.
- BlogController: This class receives requests for view posts, create comments in created posts and make full text search.

NOTE: There is one controller for each entity. Also, each controller comunicates with the entities through a "service class".

## Using the API

You could request those endpoints with the `api_token`, for example: `/api/posts?api_token=YOUR_API_KEY` .
The API key could be found in `/api/auth/token` endpoint. You have to make a POST request passing email and password by form params, then the request
will return a JSON with the `api_token` .
The backend exposes these API endpoints:

```
/api/auth/token
/api/auth/reset-password
/api/auth/change-password
/api/users     // It's exclusively available for admins
/api/posts
/api/tags
/api/categories
```

## Running tests

I recommend to change from production environment to testing environment creating a `.env.testing` (a copy of `.env` but with other MySql schema and other MongoDb database). If not, the tests will break the production database.

For change the environment:
```
php artisan migrate --seed --env=testing
```
Open console in `BloggieFigured` directory

For Windows
```
vendor\bin\phpunit
```

For Linux
```
./vendor/bin/phpunit
```

NOTE: If made the error and did not change the environment, and the application do not work, please, going from scratch the data base MySql and MongoDB.

## TO DO

- I must improve the phpunit test and add more tests.
- I should add more documentation (about use of cases, complete class diagram, PHP docs, etc).
- I would like to mprove the view, because I developed a basic front end in Veu JS.
- Security improvements
- I should improve the exceptions management and error handling.
- Implement email notifications and validations for registry and change password.

## Author

- [Ignacio Gallardo](https://www.linkedin.com/in/ignacio-martin-gallardo-64907a48/)

